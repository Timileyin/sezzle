class Calculator {
    
    constructor(type, container,operatorButtons, numberButtons, decimalPointButton, equalButton, display ) {
        this.type = type;
        this.displayEntered = "";
        this.body = document.getElementById(container);
        this.body.className = "col-md-6";
        this.display = document.getElementById(display);
        
        this.buttons = [];
        for(var i = 0; i < operatorButtons.length; i++){
            this.buttons.push(operatorButtons[i]);
        }  
        
        // 0 - 9
        for(var i = 0; i < numberButtons.length; i++){
            this.buttons.push(numberButtons[i])        
        }

        this.buttons.push(decimalPointButton)
        //Operator Buttons      
        this.buttons.push(equalButton);
    }     

    //show display()
    showDisplay(){
        this.display.type = "text";        
        this.body.append(this.display);
    }

    //Show buttons
    showButtons(){                                
        this.buttons.map((b)=>{
            this.body.append(b.fetchHtmlMarkup(this.display));
        });
    }        
  }

  class Button 
  {
    constructor(bttnTxt, bootstapClass){
        this.text = bttnTxt;
        this.bootstapClass = bootstapClass;          
    }

    fetchHtmlMarkup = (display) => {
        let buttonMarkup = document.createElement("button");
        buttonMarkup.type = "button";
        buttonMarkup.value = this.text;
        buttonMarkup.innerHTML += this.text; 
        buttonMarkup.className = this.bootstapClass;      
        buttonMarkup.addEventListener("click", () => this.onclick(display));
        return buttonMarkup;
    }
    
    onclick = (display) => {  
    }
  }



  class EqualButton extends Button{
      constructor(bttnTxt, bootstrapclass)
      {
        super(bttnTxt, bootstrapclass)
      }  
      
      onclick = (display) => {  
        let stringEvaluator = new StringEvaluator();
        display.value = stringEvaluator.solveExpression(display.value);
    }    
  }

  class NumberButton extends Button{
    constructor(bttnTxt, bootstrapclass){
        super(bttnTxt, bootstrapclass)
    }

    onclick = (display) => {
        display.value += this.text;
    }       
  }
  

  class OperatorButton extends Button{
    constructor(bttnTxt, bootstrapclass){
        super(bttnTxt, bootstrapclass)
    }

    onclick = (display) => {  
        display.value += this.text;
    }

  }

  class StringEvaluator{
      constructor(str){
          this.str = str;
      }

      solveSubExpression(arr){
        arr = arr.slice();
        debugger;
        while(arr.length-1){

            switch(arr[1]){
                case "*":
                    arr[0] = arr[0] * arr[2];
                    break;
                case "-":
                    arr[0] = arr[0] - arr[2];
                    break;
                case "+":
                    arr[0] = arr[0] + arr[2];
                    break;
                case "/":
                    arr[0] = arr[0] / arr[2];
                    break;
            }
            
            arr.splice(1,1);
            arr.splice(1,1);
        }
        
        return arr[0];
      }
      
    solveExpression(eq) {
        let res = eq.split(/(\+|-)/g).map(x => x.trim().split(/(\*|\/)/g).map(a => a.trim()));                        
        res = res.map(x => this.solveSubExpression(x));
        return this.solveSubExpression(res) //at last evaluating + and -                
    }
  }

  //class ScientificButton 